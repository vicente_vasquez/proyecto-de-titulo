# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('elegir_categoria/', views.elegir_categoria, name='elegir_categoria'),   
    path('publicar/<str:categoria_nombre>/', views.publicar, name="publicar"),     
    #urls tienda
    path('search/', views.search, name="search"),
    path('advanced_search/', views.advanced_search, name="advanced_search"),    
    path('',views.home, name="home"),     

    path('create_Profile/', views.create_Profile, name="create_Profile"),    
    path('addcomment/<int:id>', views.addcomment, name='addcomment'),    
    path('product/<int:id>/', views.product_detail, name="product_detail"),
    path('fav/<id>/', views.favourite_add, name="favourite_add"),    
    path('modificar_producto/<id>/', views.modificar_producto, name="modificar_producto"),
    path('update_profile/<id>/', views.update_profile, name="update_profile"),    
    path('eliminar_producto/<id>/',views.eliminar_producto, name="eliminar_producto"),
    path('eliminar_cuenta/<id>/',views.eliminar_cuenta, name="eliminar_cuenta"),    
    path('login/', views.login, name='login'),
    path('registro/', views.registro_usuario, name='registro_usuario'),
    path('category/<id>/', views.category_products, name="category_products"),      
    path('account/<id>/',views.account, name="account"),   
    path('account_products/<id>/',views.account_products, name="account_products"),   
    path('account_saved_products/',views.account_saved_products, name="account_saved_products"),            
    path('elegir_comprador/<id>/<idd>/',views.elegir_comprador, name="elegir_comprador"),
    path('confirmar_ofrecer_permuta/<id>/<idd>/',views.confirmar_ofrecer_permuta, name="confirmar_ofrecer_permuta"),    
    path('no_dialogs/', views.no_dialogs, name='no_dialogs'), 
]
urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
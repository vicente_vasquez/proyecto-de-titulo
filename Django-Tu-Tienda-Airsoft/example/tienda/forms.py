from django import forms
from django.forms import ModelForm, CharField, TextInput
from .models import Articulos, Comment, Perfil
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from ckeditor_uploader.widgets import CKEditorUploadingWidget  #,CKEditorWidget


class FormularioArticulo(ModelForm):
    #nombre = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))

    nombre= forms.CharField(
        label="Nombre",
        required=True,
        min_length=2,
        max_length=160,
        )
    permuta=forms.BooleanField(
        required=False,
        help_text="Marcar para establecer que tu producto acepta ser intercambiado ademas de ser vendido, recibiras mensajes personalizados de los productos que te ofrezcan para intercambiar"
        )
    imagen_principal=forms.ImageField(
        required=True, 
        help_text="Esta es la imagen con la que se mostrara tu producto en el inicio o en las busquedas"
        )        
    incluye_accesorios=forms.BooleanField(
        label="Incluye accesorios",
        required=False
    )
    potencia=forms.IntegerField(
        label="Potencia",
        required=False,
        min_value=1,
        help_text="Valor de medicion: FPS")
    distancia=forms.IntegerField(
        label="Distancia",
        required=False,
        min_value=1,
        help_text="Valor de medicion: metros aproximados")        
    #descripcion = forms.CharField(label="Descripción" ,widget=CKEditorUploadingWidget())  
    class Meta:
        model = Articulos
        fields = [
            'nombre','precio','descripcion','permuta','imagen_principal','imagen_extra_1','imagen_extra_2',
            'imagen_extra_3','imagen_extra_4','imagen_extra_5',
            'potencia',
            'distancia',
            'incluye_accesorios',
            'talla',
            'material'
            ]

class CustomUserForm(UserCreationForm):
    first_name=forms.CharField(
        label="Nombre",
        required=True
        )
    last_name=forms.CharField(
        label="Apellidos",
        required=True
        )
    email=forms.EmailField(
        label="Correo Electronico",
        required=True
        )
    password1=forms.CharField(
        label="Contraseña",
        min_length=8,
        widget=forms.PasswordInput(),
        error_messages= {'invalid_choice': ('is not a valid choice.'),
    'null': ('This field cannot be null.'),
    'blank':('This field cannot be blank.'),},
        help_text="<ul><li>Su contraseña no puede asemejarse tanto a su otra información personal.</li><li>Su contraseña debe contener al menos 8 caracteres.</li><li>Su contraseña no puede ser una clave utilizada comunmente.</li><li>Su contraseña no puede ser completamente numérica.</li></ul>"
        )
        
    #,help_text="Su contraseña no puede asemejarse tanto a su otra información personal.Su contraseña debe contener al menos 8 caracteres.<ul>Su contraseña no puede ser una clave utilizada comunmente.<p>Su contraseña no puede ser completamente numérica.")


    class Meta:
        model=User
        fields= ['first_name', 'last_name', 'email', 'username', 'password1', 'password2']
    
class SearchForm(forms.Form):
    query = forms.CharField(max_length=30)
    catid = forms.IntegerField()

class FormularioComentario(ModelForm):
    class Meta:
        model = Comment
        fields = ['calificar_producto', 'calificar_vendedor','comentario']

class FormularioPerfil(ModelForm):
    telefono = forms.CharField(  
        empty_value=None,    
        min_length=8,
        max_length=8,        
        label="Telefono    (+56 9)",
        #widget=forms.TextInput(attrs={'type':'number', 'placeholder': '12345678'}),
        help_text="Ejemplo: 12345678",
        required=False
        )
    nombre_clave = forms.CharField(
        #widget=forms.TextInput(attrs={'placeholder': 'algo'}),
        required=False
        )  
    class Meta:        
        model = Perfil
        fields = ['foto_de_perfil','genero','telefono','region','nombre_clave','escuadron','lugar_de_encuentro','arma_favorita','descripcion']      
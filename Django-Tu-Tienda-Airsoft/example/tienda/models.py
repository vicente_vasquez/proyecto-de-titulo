from django.db import models
#imports tienda
from django.contrib.auth.models import User
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.validators import MinLengthValidator
#modelos tienda
class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    OPCIONES_GENERO = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
        ('O', 'Otro')  # hay que ser inclusivos
    )  
    Armas = (
        ('a', 'Pistola'),
        ('b', 'Fusil'),
        ('c', 'Ametralladora'),
        ('d', 'Rifle'),
        ('e', 'Escopeta'),
        ('f', 'Otros'),
    )
    Regiones = (
        ('I', 'Arica'),
        ('II', 'Tarapacá'),
        ('III', 'Antofagasta'),
        ('IV', 'Coquimbo'),
        ('V', 'Valparaíso'),
        ('VI', 'Metropolitana de Santiago'),
        ('VII', 'Libertador General Bernardo O Higgins'),
        ('VIII', 'Antofagasta'), 
        ('IX', 'Maule'),       
        ('X', 'Ñuble'),
        ('XI', 'Biobío'),
        ('XII', 'La Araucanía'),
        ('XIII', 'Los Ríos'),  
        ('XIV', 'Los Lagos'),       
        ('XV', 'Aysén del General Carlos Ibáñez del Campo'),
        ('XVI', 'Magallanes y de la Antártica Chilena'),                               
    )  
    foto_de_perfil=models.ImageField(upload_to="user_profile", null=True, blank=True)
    telefono=models.CharField(max_length=8, validators=[MinLengthValidator(8)],null=True, blank=True) 
    region= models.CharField(max_length=4, choices=Regiones, null=True, blank=True)
    genero = models.CharField(max_length=1, choices=OPCIONES_GENERO, null=True, blank=True)      
    nombre_clave=models.CharField(max_length=30, null=True, blank=True)
    escuadron=models.CharField(max_length=30, null=True, blank=True)  
    lugar_de_encuentro=models.CharField(max_length=30, null=True, blank=True)        
    descripcion = models.TextField(max_length=500, null=True, blank=True)
    arma_favorita = models.CharField(max_length=1, choices=Armas, null=True, blank=True)
    def __str__(self):
        return self.nombre_clave    

class Category(MPTTModel):
    name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['name']
    def __str__(self):
        return self.name 

    def __str__(self):
        full_path=[self.name]
        k= self.parent

        while k is not None:
            full_path.append(k.name)
            k= k.parent
        return ' / '.join(full_path[::-1])

class Articulos(models.Model):
    nombre=models.CharField(max_length=160)
    categoria=models.ForeignKey(Category, on_delete=models.CASCADE)
    #atributos especiales de cada categoria
    largo_del_cañon=models.PositiveIntegerField(default=None, null=True, blank=True)
    potencia=models.PositiveIntegerField(default=None, null=True, blank=True) 
    distancia=models.PositiveIntegerField(default=None, null=True, blank=True)  
    incluye_accesorios=models.BooleanField(verbose_name=("Incluye accesorios"), default=False, null=True, blank=True)     
    #botas
    tallas = (
        ('S', 'S'),
        ('M', 'M'),
        ('L', 'L'),        
        ('XL', 'XL')  # hay que ser inclusivos
    )      
    talla=models.CharField(max_length=2, choices=tallas, default=None, null=True, blank=True)
    material=models.CharField(max_length=30, default=None, null=True, blank=True)
    #para todos los productos
    descripcion=RichTextUploadingField(null=True, blank=True)
    precio= models.PositiveIntegerField()
    #cantidad=models.IntegerField()
    imagen_principal=models.ImageField(upload_to="home")
    imagen_extra_1=models.ImageField(upload_to="images/", null=True, blank=True)    
    imagen_extra_2=models.ImageField(upload_to="images/", null=True, blank=True)    
    imagen_extra_3=models.ImageField(upload_to="images/", null=True, blank=True)    
    imagen_extra_4=models.ImageField(upload_to="images/", null=True, blank=True)    
    imagen_extra_5=models.ImageField(upload_to="images/", null=True, blank=True)    
    autor=models.ForeignKey(User, on_delete=models.CASCADE,related_name='articulo')
    comprador=models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=True, blank=True, related_name='comprador')      
    favourites = models.ManyToManyField( User, related_name='favourite', default=None, blank=True)
    vendido = models.BooleanField(verbose_name=("Vendido"), default=False)
    permuta = models.BooleanField(verbose_name=("Permuta"), default=False)
    created=models.DateField(auto_now_add=True)
    updated=models.DateField(auto_now_add=True)     
  
    def __str__(self):
        return str(self.nombre) + ": $" + str(self.precio)

    def get_absolute_url(self):
        return "/product/"+str(self.id)

class Comment(models.Model):
    product = models.ForeignKey(Articulos, default=None, on_delete=models.CASCADE)
    userComentando = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_comentando')
    userCalificado = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_calificado')
    comentario = models.CharField(max_length=250,blank=True)
    calificar_producto = models.IntegerField()
    calificar_vendedor = models.IntegerField()
    create_at=models.DateTimeField(auto_now_add=True)
    update_at=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comentario
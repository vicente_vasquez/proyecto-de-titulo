import time
from locust import HttpUser, task, between


class WebsiteUser(HttpUser):
    wait_time = between(1, 5)

    @task
    def home_page(self):
        self.client.get(url="/")
    
    @task
    def product_page(self):
        self.client.get(url="/product/2/")
        
    @task
    def account_page(self):
        self.client.get(url="/account/2/")        
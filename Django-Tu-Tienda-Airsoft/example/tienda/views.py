#imports tienda
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from .models import Articulos, Category, User, Comment, Perfil
from django_private_chat.models import Dialog, Message
from .forms import FormularioArticulo, CustomUserForm, SearchForm, FormularioPerfil, FormularioComentario
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.contrib import messages
from django.forms import modelformset_factory
from django.template import RequestContext
#from pyexpat.errors import messages

#views tienda
def home(request):   
    products_slider= Articulos.objects.filter(vendido=False).order_by('?')[:4]    
    ultimos_articulos=Articulos.objects.filter(vendido=False).order_by('-id')[:4]
    articulos_random=Articulos.objects.filter(vendido=False).order_by('?')[:4]

    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes=Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2= Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,

        'products_slider': products_slider, 
        'ultimos_articulos': ultimos_articulos,
        'articulos_random': articulos_random 
        }
    return render(request, "tienda/home.html",context)
#-----------------------------------------------------------------------------
@login_required
def no_dialogs(request):
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,
        }    
    return render(request, 'tienda/no_dialogs.html',context)
#-----------------------------------------------------------------
def elegir_categoria(request):
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0    


    category = request.GET.get('category')
    if is_valid_queryparam(category) and category != 'Elegir categoria':
        buscar=Category.objects.get(id=category)
        nombre=buscar.name
        #qs = qs.filter(categoria__id=category) 
        return HttpResponseRedirect("/publicar/"+str(nombre))   

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,        
    }  
    return render(request, 'tienda/elegir_categoria.html', context)  
#-----------------------------------------------------------------------------    
def publicar(request, categoria_nombre):    
    categorias=Category.objects.all()
    categoria=Category.objects.get(name=str(categoria_nombre))
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    data={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,  

        'formularioArticulo': FormularioArticulo, 
        'categoria':categoria
    }     
    if request.method == 'POST':
        formularioArticulo =  FormularioArticulo(request.POST, files=request.FILES)

        if  formularioArticulo.is_valid():
            post_form =  formularioArticulo.save(commit=False)
            post_form.autor = request.user
            post_form.categoria= categoria
            post_form.save()
            messages.success(request, "Tu producto se ha publicado exitosamente")             
            return HttpResponseRedirect("/account_products/"+str(request.user.id))  
        else:
            messages.error(request, "No se ha publicado tu producto: formulario mal completado") 
    return render(request, 'tienda/publicar.html',data)
#----------------------------------------------------------------------------------------------- 
def create_Profile(request):

    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    data={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,

        'formularioPerfil': FormularioPerfil,            
    } 

    if request.method == 'POST':
        formularioPerfil =  FormularioPerfil(request.POST, files=request.FILES)

        if  formularioPerfil.is_valid():
            post_form =  formularioPerfil.save(commit=False)
            post_form.user = request.user
            post_form.save()
            messages.success(request, "Datos de perfil guardados exitosamente")            
            return HttpResponseRedirect("/account/"+str(request.user.id))
        else:
            messages.error(request, "No se han guardado los datos de tu perfil")  

    return render(request, 'tienda/create_Profile.html', data)
#----------------------------------------------------------------------------
def addcomment(request,id):

    url = request.META.get('HTTP_REFERER')  # ultima url visitada (la del producto)
    
    if request.method == 'POST' and 'btnform8' in request.POST: #(boton de comentar)
        
        form = FormularioComentario(request.POST)
        if form.is_valid():
            data = Comment()  # crear relacion con el modelo
            data.comentario = form.cleaned_data['comentario']
            data.calificar_producto = form.cleaned_data['calificar_producto']
            data.calificar_vendedor = form.cleaned_data['calificar_vendedor']            
            data.product_id=id
            producto = Articulos.objects.get(id=id)            
            data.userCalificado=producto.autor
            current_user=request.user
            data.userComentando_id=current_user.id
            data.save()  # crear comentario
            messages.success(request, "Tu comentario se ha publicado exitosamente")            
            return HttpResponseRedirect(url)
        else:
            messages.error(request, "Comentario no publicado: debes completar los campos de calificación")            

    return HttpResponseRedirect(url)
#---------------------------------------------------------------------------------------------------------
def modificar_producto(request, id): 
    articulo = Articulos.objects.get(id=id)
    categoria=articulo.categoria
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    data={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,

        'articulo':articulo,
        'categoria':categoria,
        'formularioArticulo': FormularioArticulo(instance=articulo),             
    }   
    if request.method == 'POST':
        formularioArticulo =  FormularioArticulo(data=request.POST, instance=articulo, files=request.FILES)
        if  formularioArticulo.is_valid():
            post_form =  formularioArticulo.save(commit=False)
            post_form.autor = request.user
            post_form.categoria= categoria
            post_form.save()
            data['form'] = FormularioArticulo(instance=Articulos.objects.get(id=id))
            messages.success(request, "Datos de producto modificados exitosamente")            
            return redirect(to="/product/"+str(articulo.id))
        else:
            messages.error(request, "No se han modificado los datos de producto")  
    else:
        formularioArticulo = FormularioArticulo()

    return render(request, 'tienda/modificar_producto.html',data)

#----------------------------------------------
def update_profile(request, id): 

    perfil = Perfil.objects.get(id=id)
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes=Message.objects.filter(read=False).exclude(sender=request.user)
        chats1 = Dialog.objects.filter(opponent=request.user)
        chats2 = Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    data={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,        

        'formularioPerfil': FormularioPerfil(instance=perfil),            
    } 
    if request.method == 'POST':
        formulario = FormularioPerfil(data=request.POST, instance=perfil, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Datos de perfil modificados correctamente"
            data['form'] = FormularioPerfil(instance=Perfil.objects.get(id=id))
            messages.success(request, "Datos de perfil modificados exitosamente")            
            return HttpResponseRedirect("/account/"+str(request.user.id))
        else:
            messages.error(request, "No se han modificado los datos de tu perfil")            


    return render(request, 'tienda/update_profile.html', data)
#----------------------------------------------
def product_detail(request, id): 
    decision=0
    terminado=0
    categorias=Category.objects.all()
    articulo = Articulos.objects.get(id=id)

    fav=False
    if articulo.favourites.filter(id=request.user.id).exists():
        fav=True
    notificar=0 #NO HAY NUEVOS MENSAJES

    chats=None
    chats1=None    
    chats2=None    
    comentario=None
    permutar=None
    comentarios=len(Comment.objects.filter(product=articulo))
    if comentarios==1:
        comentario=Comment.objects.get(product=articulo)
    
    if request.user.is_authenticated:
        permutar = Articulos.objects.filter(autor=request.user).exclude(permuta=False)        
        chats=1
        mensajes=Message.objects.filter(read=False).exclude(sender=request.user) #LISTA DE MENSAJES NO LEIDOS, SIN CONTAR LOS QUE EL REQUEST USER ENVIO
        chats1 = Dialog.objects.filter(opponent=request.user)#LISTA DE CHATS CON EL REQUES.USER COM OPPONET
        chats2 = Dialog.objects.filter(owner=request.user)   #LISTA DE CHATS CON EL REQUEST.USER COMO OWNER
        for i in range(0, len(mensajes)):#EN EL RANGO DEL LARGO DE MENSAJES
            if mensajes[i].dialog in chats1:#SI EXISTE UNO DE LOS MENSAJES NO LEIDOS EN LOS DIALOGS
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0
        buscar_emisor = Dialog.objects.filter(owner=request.user,  opponent=articulo.autor) #existe admin->alex?
        buscar_receptor=Dialog.objects.filter(owner=articulo.autor,  opponent=request.user) #existe alex->admin?
        buscar_mensaje= Message.objects.filter(sender=request.user, text="Hola, me gustaria comprar tu producto llamado "+str(articulo.nombre))#existe un mensaje de este producto en especifico?
        existe_mensaje=len(buscar_mensaje)
        existe_emisor=len(buscar_emisor) 
        existe_receptor=len(buscar_receptor)   
    #caso 0 el usuario no ha iniciado sesion
    #desicion=0 pedirle que inicie sesion                           
        if existe_emisor==0 and existe_receptor==0:   #caso 1 no exite chat en ninguna direccion
            decision=1 #se debe crear el chat enviar el mensaje predefinido
        elif existe_emisor==1 and existe_mensaje==0:  #caso 2 existe admin->alex y no existe un mensaje de parte de admin por este producto en especifico
            decision=2 #no crear chat, solo enviar mensaje predefinido, reederigir a /receptor
        elif existe_receptor==1 and existe_mensaje==0:#caso 3 existe alex->admin y no existe un mensaje de admin por este producto
            decision=3 #no crear chat, solo enviar mensaje predefinido, reederigir a /emisor
        elif existe_emisor==1 and existe_mensaje==1:  #caso 4 existe el chat y si existe el mensaje de este producto
            decision=4 #no crear chat, no enviar mensaje, reederigir a /receptor
        elif existe_receptor==1 and existe_mensaje==1:#caso 5 existe el chat alex->admin y si existe el mensaje de este producto
            decision=5 #no crear chat, no enviar mensaje, reederigir a /emisor

    if articulo.vendido==True:
        terminado=1

    context= {
        'notificar':notificar,
        'usuario_log':request.user, 
        'autor':articulo.autor,
        'articulos':articulo,
        'categorias': categorias,

        'decision':decision,
        'chats':chats,
        'chats1':chats1,
        'chats2':chats2,
        'permutar':permutar,
        'terminado':terminado,
        'comentario':comentario,
        'comentarios':comentarios,
        'fav':fav,
        'formularioComentario': FormularioComentario,        
    }
    if request.method=='POST' and 'btnform1' in request.POST:   #caso 1
        Dialog.objects.create(owner=request.user, opponent=articulo.autor) #crear chat como admin->alex
        dialogo=get_object_or_404(Dialog, owner=request.user ,opponent=articulo.autor) #el receptor es el dueño del producto
        Message.objects.create(dialog=dialogo, sender=request.user, text="Hola, me gustaria comprar tu producto llamado "+str(articulo.nombre))              
        return redirect(to="/dialogs/"+str(articulo.autor))
    elif request.method == "POST" and 'btnform2' in request.POST:#caso2    
        dialogo=get_object_or_404(Dialog, owner=request.user ,opponent=articulo.autor) #no se creo el chat, el receptor es el dueño del producto
        Message.objects.create(dialog=dialogo, sender=request.user, text="Hola, me gustaria comprar tu producto llamado "+str(articulo.nombre))              
        return redirect(to="/dialogs/"+str(articulo.autor))
    elif request.method == "POST" and 'btnform3' in request.POST:#caso3, alex te habia hablado y ahora tu le hablas por este producto    
        dialogo=get_object_or_404(Dialog, owner=articulo.autor, opponent=request.user)#no se creo el chat, el emisor es el dueño del producto
        Message.objects.create(dialog=dialogo, sender=request.user, text="Hola, me gustaria comprar tu producto llamado "+str(articulo.nombre))              
        return redirect(to="/dialogs/"+str(articulo.autor))
    if request.method == "POST" and 'btnform4' in request.POST:
        return redirect(to="/dialogs/"+str(articulo.autor))
    if request.method == "POST" and 'btnform5' in request.POST:
        return redirect(to="/dialogs/"+str(articulo.autor))
                      
    return render(request, 'tienda/product_detail.html', context)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def confirmar_ofrecer_permuta(request, id, idd):  
    decision=0    
    ofreces = Articulos.objects.get(id=id)
    articulo=Articulos.objects.get(id=idd)#el producto que quieres
    todo_bien=1
    if articulo.vendido==True or ofreces.vendido==True:#ninguno de los 2 objetos puede estar como vendido
        todo_bien=4041
    if articulo.permuta==False or ofreces.permuta==False:#ninguno de los 2 objetos puede ser no permutable
        todo_bien=4042
    if ofreces.autor != request.user:#si tu no eres el autor del producto ofrecido
        todo_bien=4043
    if ofreces.autor == articulo.autor:#no puedes ofrecer un producto a ti mismo
        todo_bien=4044
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

        buscar_emisor = Dialog.objects.filter(owner=request.user,  opponent=articulo.autor) #existe admin->alex?
        buscar_receptor=Dialog.objects.filter(owner=articulo.autor,  opponent=request.user) #existe alex->admin?
        buscar_mensaje= Message.objects.filter(sender=request.user, text="Hola, ¿te interecaria intercambiar tu producto "+str(articulo.nombre)+" por mi producto "+str(ofreces.nombre)+"?\npuedes ver mi producto en "+str(request.build_absolute_uri('/product/'+str(ofreces.id))))#existe un mensaje de este producto en especifico?
        existe_mensaje=len(buscar_mensaje)
        existe_emisor=len(buscar_emisor) 
        existe_receptor=len(buscar_receptor)   
    #caso 0 el usuario no ha iniciado sesion
    #desicion=0 pedirle que inicie sesion                           
        if existe_emisor==0 and existe_receptor==0:   #caso 1 no exite chat en ninguna direccion
            decision=1 #se debe crear el chat enviar el mensaje predefinido
        elif existe_emisor==1 and existe_mensaje==0:  #caso 2 existe admin->alex y no existe un mensaje de parte de admin por este producto en especifico
            decision=2 #no crear chat, solo enviar mensaje predefinido, reederigir a /receptor
        elif existe_receptor==1 and existe_mensaje==0:#caso 3 existe alex->admin y no existe un mensaje de admin por este producto
            decision=3 #no crear chat, solo enviar mensaje predefinido, reederigir a /emisor
        elif existe_emisor==1 and existe_mensaje==1:  #caso 4 existe el chat y si existe el mensaje de este producto
            decision=4 #no crear chat, no enviar mensaje, reederigir a /receptor
        elif existe_receptor==1 and existe_mensaje==1:#caso 5 existe el chat alex->admin y si existe el mensaje de este producto
            decision=5 #no crear chat, no enviar mensaje, reederigir a /emisor
    
    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,

        'articulo':articulo,
        'ofreces':ofreces,
        'decision':decision,
        'todo_bien':todo_bien
    }
    if request.method == "POST" and 'btnform1' in request.POST:
        Dialog.objects.create(owner=request.user, opponent=articulo.autor) #crear chat como admin->alex
        dialogo=get_object_or_404(Dialog, owner=request.user ,opponent=articulo.autor) #el receptor es el dueño del producto
        Message.objects.create(dialog=dialogo, sender=request.user, text="Hola, ¿te interecaria intercambiar tu producto "+str(articulo.nombre)+" por mi producto "+str(ofreces.nombre)+"?\npuedes ver mi producto en "+str(request.build_absolute_uri('/product/'+str(ofreces.id))))
        return redirect(to="/dialogs/"+str(articulo.autor))
    elif request.method == "POST" and 'btnform2' in request.POST:#caso2    
        dialogo=get_object_or_404(Dialog, owner=request.user ,opponent=articulo.autor) #no se creo el chat, el receptor es el dueño del producto
        Message.objects.create(dialog=dialogo, sender=request.user, text="Hola, ¿te interecaria intercambiar tu producto "+str(articulo.nombre)+" por mi producto "+str(ofreces.nombre)+"?\npuedes ver mi producto en "+str(request.build_absolute_uri('/product/'+str(ofreces.id))))
        return redirect(to="/dialogs/"+str(articulo.autor))
    elif request.method == "POST" and 'btnform3' in request.POST:#caso3, alex te habia hablado y ahora tu le hablas por este producto    
        dialogo=get_object_or_404(Dialog, owner=articulo.autor, opponent=request.user)#no se creo el chat, el emisor es el dueño del producto
        Message.objects.create(dialog=dialogo, sender=request.user, text="Hola, ¿te interecaria intercambiar tu producto "+str(articulo.nombre)+" por mi producto "+str(ofreces.nombre)+"?\npuedes ver mi producto en "+str(request.build_absolute_uri('/product/'+str(ofreces.id))))
        return redirect(to="/dialogs/"+str(articulo.autor))       
    return render(request, 'tienda/confirmar_ofrecer_permuta.html', context)    
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def elegir_comprador(request, id, idd):   
    comprador = User.objects.get(id=id)    
    articulo=Articulos.objects.get(id=idd)    
    todo_bien=1
    if articulo.vendido==True:#si el produco ya fue marcado como vendido
        todo_bien=4045
    if articulo.autor != request.user:#si tu no eres el dueño de este producto
        todo_bien=4046
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,

        'comprador': comprador,
        'articulo':articulo,
        'todo_bien':todo_bien
    }
    if request.method == "POST" and 'btnform0' in request.POST:
        dialogo=get_object_or_404(Dialog,owner=comprador, opponent=request.user) #no se creo el chat, el receptor es el dueño del producto
        Message.objects.create(dialog=dialogo, sender=request.user, text="Ya puedes valorarme como vendedor y sobre mi producto "+str(articulo.nombre)+"\n "+str(request.build_absolute_uri('/product/'+str(articulo.id)))) 
        articulo.vendido=True
        articulo.save()
        articulo.comprador=comprador
        articulo.save() 
        return redirect(to="/dialogs/"+str(comprador))
        #return redirect(to="/product/"+str(articulo.id))
    return render(request, 'tienda/elegir_comprador.html', context)
#----------------------------------------------
@ login_required
def favourite_add(request, id):
    url = request.META.get('HTTP_REFERER') 
    #articulo = get_object_or_404(Articulos, id=id)
    articulo= Articulos.objects.get(id=id)    
    if articulo.favourites.filter(id=request.user.id).exists():#aca debe estar el error
        articulo.favourites.remove(request.user)
        return redirect(to=url)
    else:
	    articulo.favourites.add(request.user)    

    return redirect(to=url)
#----------------------------------------------    
def category_products(request, id): 
    articulos = Articulos.objects.filter(categoria_id=id)
    categoria =Category.objects.get(id=id)
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    qs =articulos  
    nombre_contains_query=request.GET.get('nombre_contains')
    precio_count_min =request.GET.get('precio_count_min')
    precio_count_max =request.GET.get('precio_count_max')  
    date_min = request.GET.get('date_min')
    date_max = request.GET.get('date_max')
    category = request.GET.get('category')
    vendido = request.GET.get('vendido')
    not_vendido = request.GET.get('notVendido') 
    permuta = request.GET.get('permuta')
    not_permuta = request.GET.get('notPermuta')    

    if is_valid_queryparam(nombre_contains_query):
        qs = qs.filter(nombre__icontains=nombre_contains_query)

    if is_valid_queryparam(precio_count_min):
        qs = qs.filter(precio__gte=precio_count_min)

    if is_valid_queryparam(precio_count_max):
        qs = qs.filter(precio__lt=precio_count_max)

    if is_valid_queryparam(date_min):
        qs = qs.filter(created__gte=date_min)

    if is_valid_queryparam(date_max):
        qs = qs.filter(created__lt=date_max)

    if is_valid_queryparam(category) and category != 'Elegir categoria':
        qs = qs.filter(categoria__id=category)

    if vendido == 'on':
        qs = qs.filter(vendido=True)

    elif not_vendido == 'on':
        qs = qs.filter(vendido=False)  

    if permuta == 'on':
        qs = qs.filter(permuta=True)

    elif not_permuta == 'on':
        qs = qs.filter(permuta=False)  

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,        

        'queryset':qs,

        'articulos': articulos, 
        'categoria':categoria
        }            
    return render(request, 'tienda/categoria_productos.html', context)
#----------------------------------------------
def account(request, id):
    
    user= User.objects.get(id=id)
    existe_perfil= Perfil.objects.filter(user=user)
    genero=None
    region=""
    arma_favorita=""
    perfil=None
    id_perfil=None
    if len(existe_perfil)==1:
        id_perfil=existe_perfil[0].id
        perfil= Perfil.objects.get(user=user) 
        genero= perfil.get_genero_display()
        region= perfil.get_region_display() 
        arma_favorita=perfil.get_arma_favorita_display() 

    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0
    calificacion_vendedor=None
    calificacion_producto=None    
    comentarios= Comment.objects.filter(userCalificado=user)
    cantidad_comentarios=len(comentarios)
    if cantidad_comentarios==1:
        calificacion_vendedor=comentarios[0].calificar_vendedor
        calificacion_producto=comentarios[0].calificar_producto
    if cantidad_comentarios>1:
        total=0
        for i in range(0,cantidad_comentarios):
            nota=comentarios[i].calificar_vendedor
            total=total+nota
        calificacion_vendedor=int(total/cantidad_comentarios)
        total=0
        for i in range(0,cantidad_comentarios):
            nota=comentarios[i].calificar_producto
            total=total+nota
        calificacion_producto=int(total/cantidad_comentarios) 

    cantidad_productos_publicados=len(Articulos.objects.filter(autor=user))
    cantidad_productos_vendidos=len(Articulos.objects.filter(autor=user).exclude(vendido=False))
    cantidad_comentarios_realizados=len(Comment.objects.filter(userComentando=user))

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,

        'calificacion_vendedor':calificacion_vendedor,
        'cantidad_comentarios':cantidad_comentarios,
        'calificacion_producto':calificacion_producto,
        'cantidad_productos_publicados':cantidad_productos_publicados,
        'cantidad_productos_vendidos':cantidad_productos_vendidos,
        'cantidad_comentarios_realizados':cantidad_comentarios_realizados,

        'existe_perfil':len(existe_perfil),
        'usuario_log':request.user,
        'users': user, 
        'perfil': perfil,
        'genero' : genero,
        'region' : region,        
        'arma_fav':arma_favorita,
        'id_perfil':id_perfil
    }   
    return render(request, 'tienda/account.html', context)
#-----------------------------------------------------------
def account_products(request, id):
    articulo= Articulos.objects.filter(autor=id)       
    user= User.objects.get(id=id)

    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0       

    qs =articulo  
    nombre_contains_query=request.GET.get('nombre_contains')
    precio_count_min =request.GET.get('precio_count_min')
    precio_count_max =request.GET.get('precio_count_max')  
    date_min = request.GET.get('date_min')
    date_max = request.GET.get('date_max')
    category = request.GET.get('category')
    vendido = request.GET.get('vendido')
    not_vendido = request.GET.get('notVendido') 
    permuta = request.GET.get('permuta')
    not_permuta = request.GET.get('notPermuta')    

    if is_valid_queryparam(nombre_contains_query):
        qs = qs.filter(nombre__icontains=nombre_contains_query)

    if is_valid_queryparam(precio_count_min):
        qs = qs.filter(precio__gte=precio_count_min)

    if is_valid_queryparam(precio_count_max):
        qs = qs.filter(precio__lt=precio_count_max)

    if is_valid_queryparam(date_min):
        qs = qs.filter(created__gte=date_min)

    if is_valid_queryparam(date_max):
        qs = qs.filter(created__lt=date_max)

    if is_valid_queryparam(category) and category != 'Elegir categoria':
        qs = qs.filter(categoria__id=category)

    if vendido == 'on':
        qs = qs.filter(vendido=True)

    elif not_vendido == 'on':
        qs = qs.filter(vendido=False)  

    if permuta == 'on':
        qs = qs.filter(permuta=True)

    elif not_permuta == 'on':
        qs = qs.filter(permuta=False)  

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,        

        'queryset':qs,

        'users': user,     
        "articulos": articulo, 
    }
    return render(request, 'tienda/account_products.html', context) 
#-----------------------------------------------------------------------
def account_saved_products(request):
    user= request.user
    articulo= Articulos.objects.filter(favourites=user)     

    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0
                   
    qs =articulo  
    nombre_contains_query=request.GET.get('nombre_contains')
    precio_count_min =request.GET.get('precio_count_min')
    precio_count_max =request.GET.get('precio_count_max')  
    date_min = request.GET.get('date_min')
    date_max = request.GET.get('date_max')
    category = request.GET.get('category')
    vendido = request.GET.get('vendido')
    not_vendido = request.GET.get('notVendido') 
    permuta = request.GET.get('permuta')
    not_permuta = request.GET.get('notPermuta')    

    if is_valid_queryparam(nombre_contains_query):
        qs = qs.filter(nombre__icontains=nombre_contains_query)

    if is_valid_queryparam(precio_count_min):
        qs = qs.filter(precio__gte=precio_count_min)

    if is_valid_queryparam(precio_count_max):
        qs = qs.filter(precio__lt=precio_count_max)

    if is_valid_queryparam(date_min):
        qs = qs.filter(created__gte=date_min)

    if is_valid_queryparam(date_max):
        qs = qs.filter(created__lt=date_max)

    if is_valid_queryparam(category) and category != 'Elegir categoria':
        qs = qs.filter(categoria__id=category)

    if vendido == 'on':
        qs = qs.filter(vendido=True)

    elif not_vendido == 'on':
        qs = qs.filter(vendido=False)  

    if permuta == 'on':
        qs = qs.filter(permuta=True)

    elif not_permuta == 'on':
        qs = qs.filter(permuta=False)  

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,        

        'queryset':qs,

        'users': user,     
        "articulos": articulo, 
    }
    return render(request, 'tienda/account_saved_products.html', context)     
#----------------------------------------------
def eliminar_producto(request, id):

    articulo= Articulos.objects.get(id=id)
    if request.user==articulo.autor:
        articulo.delete()
    else:
        return redirect(to="/")

    return redirect(to="/account_products/"+str(articulo.autor.id))
#----------------------------------------------
def eliminar_cuenta(request, id):

    usuario= User.objects.get(id=id)
    if request.user==usuario:
        usuario.delete()
    else:
        return redirect(to="/")

    return redirect(to="/")
#----------------------------------------------
def registro_usuario(request):

    context = {
        'form':CustomUserForm(),
    }

    if request.method == 'POST':
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            username= formulario.cleaned_data['username']
            password= formulario.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, 'Cuenta registrada existosamente')            
            return redirect(to="/account/"+str(request.user.id))
        else:
            messages.error(request, 'Formato invalido')
    return render(request, 'registration/registrar.html', context)
#----------------------------------------------
def search(request):
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes= Message.objects.filter(read=False).exclude(sender=request.user)
        chats1= Dialog.objects.filter(opponent=request.user)
        chats2=Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            query = form.cleaned_data['query']
            catid = form.cleaned_data['catid']
            if catid==0:
                qs=Articulos.objects.filter(nombre__icontains=query)
            else:
                qs = Articulos.objects.filter(nombre__icontains=query, categoria_id=catid)            
            context={
                'categorias': categorias,
                'chats':chats,
                'notificar':notificar,

                'queryset': qs,
                }
            return render(request, 'tienda/search_products.html', context)

    qs =Articulos.objects.all()    
    nombre_contains_query=request.GET.get('nombre_contains')
    precio_count_min =request.GET.get('precio_count_min')
    precio_count_max =request.GET.get('precio_count_max')  
    date_min = request.GET.get('date_min')
    date_max = request.GET.get('date_max')
    category = request.GET.get('category')
    vendido = request.GET.get('vendido')
    not_vendido = request.GET.get('notVendido') 
    permuta = request.GET.get('permuta')
    not_permuta = request.GET.get('notPermuta')   

    if is_valid_queryparam(nombre_contains_query):
        qs = qs.filter(nombre__icontains=nombre_contains_query)

    if is_valid_queryparam(precio_count_min):
        qs = qs.filter(precio__gte=precio_count_min)

    if is_valid_queryparam(precio_count_max):
        qs = qs.filter(precio__lt=precio_count_max)

    if is_valid_queryparam(date_min):
        qs = qs.filter(created__gte=date_min)

    if is_valid_queryparam(date_max):
        qs = qs.filter(created__lt=date_max)

    if is_valid_queryparam(category) and category != 'Elegir categoria':
        qs = qs.filter(categoria__id=category)

    if vendido == 'on':
        qs = qs.filter(vendido=True)

    elif not_vendido == 'on':
        qs = qs.filter(vendido=False)  

    if permuta == 'on':
        qs = qs.filter(permuta=True)

    elif not_permuta == 'on':
        qs = qs.filter(permuta=False)  

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,        

        'queryset':qs,
    }  
    return render(request, 'tienda/advanced_search.html', context)    
#----------------------------------------------
def is_valid_queryparam(param):
    return param!= '' and param is not None
#----------------------------------------------
def advanced_search(request):
    categorias=Category.objects.all()
    chats=0
    notificar=0       
    if request.user.is_authenticated:
        chats=1
        mensajes=Message.objects.filter(read=False).exclude(sender=request.user)
        chats1 = Dialog.objects.filter(opponent=request.user)
        chats2 = Dialog.objects.filter(owner=request.user)
        for i in range(0, len(mensajes)):
            if mensajes[i].dialog in chats1:
                notificar=1
            elif mensajes[i].dialog in chats2:
                notificar=1
        if len(chats1)==0 and len(chats2)==0:
            chats=0

    qs =Articulos.objects.all()    
    nombre_contains_query=request.GET.get('nombre_contains')
    precio_count_min =request.GET.get('precio_count_min')
    precio_count_max =request.GET.get('precio_count_max')  
    date_min = request.GET.get('date_min')
    date_max = request.GET.get('date_max')
    category = request.GET.get('category')
    vendido = request.GET.get('vendido')
    not_vendido = request.GET.get('notVendido') 
    permuta = request.GET.get('permuta')
    not_permuta = request.GET.get('notPermuta')    

    if is_valid_queryparam(nombre_contains_query):
        qs = qs.filter(nombre__icontains=nombre_contains_query)

    if is_valid_queryparam(precio_count_min):
        qs = qs.filter(precio__gte=precio_count_min)

    if is_valid_queryparam(precio_count_max):
        qs = qs.filter(precio__lt=precio_count_max)

    if is_valid_queryparam(date_min):
        qs = qs.filter(created__gte=date_min)

    if is_valid_queryparam(date_max):
        qs = qs.filter(created__lt=date_max)

    if is_valid_queryparam(category) and category != 'Elegir categoria':
        qs = qs.filter(categoria__id=category)

    if vendido == 'on':
        qs = qs.filter(vendido=True)

    elif not_vendido == 'on':
        qs = qs.filter(vendido=False)  

    if permuta == 'on':
        qs = qs.filter(permuta=True)

    elif not_permuta == 'on':
        qs = qs.filter(permuta=False)  

    context={
        'categorias': categorias,
        'chats':chats,
        'notificar':notificar,        

        'queryset':qs,
    }  
    return render(request, 'tienda/advanced_search.html', context)                      
#----------------------------------------------

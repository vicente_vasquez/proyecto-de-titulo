from django.contrib import admin
from .models import Category, Articulos, Comment, Perfil
from mptt.admin import DraggableMPTTAdmin
#admin tienda
class CategoryAdmin(DraggableMPTTAdmin):
    mptt_indent_field = "name"
    list_display = ('tree_actions', 'indented_title',
                    'related_products_count', 'related_products_cumulative_count')
    list_display_links = ('indented_title',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        # Add cumulative product count
        qs = Category.objects.add_related_count(
                qs,
                Articulos,
                'categoria',
                'products_cumulative_count',
                cumulative=True)

        # Add non cumulative product count
        qs = Category.objects.add_related_count(qs,
                 Articulos,
                 'categoria',
                 'products_count',
                 cumulative=False)
        return qs

    def related_products_count(self, instance):
        return instance.products_count
    related_products_count.short_description = 'Cantidad de productos relacionados (para esta categoria)'

    def related_products_cumulative_count(self, instance):
        return instance.products_cumulative_count
    related_products_cumulative_count.short_description = 'Cantidad de productos relacionados (para el arbol de categoria)'

class ProductoAdmin(admin.ModelAdmin):
    #list_display=("nombre", "categoria", "precio")
    search_fields=("nombre", "categoria")
    list_filter=("categoria","autor")

admin.site.register(Category, CategoryAdmin)
admin.site.register(Articulos, ProductoAdmin)
admin.site.register(Comment)
admin.site.register(Perfil)